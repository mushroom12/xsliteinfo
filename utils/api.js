//域名地址
var prefix = '';
//接口授权码
var authCode = '';
//接口访问类型
var clientType= 'wsc';
/*接口地址开始*/
//授权获取微信用户信息url
const oauthurl = prefix + '/wcsp/oauth';
//颜值分析url
const faceurl = prefix+'/rest/face/detect?clientType='+clientType+'&apiType=face&authCode='+authCode;
//魅力值分析url
const charmurl = prefix + '/rest/youtu/detect?clientType=' + clientType + '&apiType=face&authCode=' + authCode;
/*常量函数*/
function getOauthUrl() {
  return oauthurl;
}
function getFaceUrl(){
  return faceurl;
}
function getCharmUrl() {
  return charmurl;
}

/*暴露常量函数*/
module.exports.getOauthUrl = getOauthUrl;
module.exports.getFaceUrl = getFaceUrl;
module.exports.getCharmUrl = getCharmUrl;